import { Box, Button, InputBase, TextField, Typography } from "@mui/material";
import React, { useState } from "react";

import { Google, LocalPhone } from "@mui/icons-material";
import {
  ActionFunction,
  createCookie,
  Form,
  json,
  LoaderFunction,
  redirect,
} from "remix";
import { useActionData } from "remix";
import { z, ZodError } from "zod";
import {
  twilioCustomPhoneRegistration,
} from "~/twilio/otp_twilio";

export const registration_schema = z.object({
  email: z.string({ required_error: "Email field is required" }).email(),
  password: z.string({ required_error: "Password field is required" }).min(5),
  fullname: z.string({ required_error: "Full Name required" }).min(1),
  phone: z.string({ required_error: "Phone field required" }).min(1),
});
export const validation = (data: any | object) => {
  const {email,password,phone,fullname}=data
  try {
    const result = registration_schema.parse({
      email,
      password,
      phone,
      fullname
    });
    console.log("Result", result);
    return {
      email_user: result?.email,
      password_user: result?.password,
      phone_user: result?.phone,
      fullname_user: result?.fullname,
      err: false,
    };
  } catch (error) {
    if (error instanceof ZodError) {
      const e = error as ZodError;
    data.email&&email,
    data.password&&password,
    data.phone&&phone,
    data.fullname&&fullname
      return {
        errContent: e,
        email,
        password,
        phone,
        fullname,
        err: true,
        status: 400,
        message: "Validation Error",
      };
    }
  }
};

export type Reg_Result = {
  err?: boolean;
  errContent?: any | unknown;
  message?: string;
  status?: number;
  error?: any | unknown;
  phone_user?: any;
  fullname_user?: any;
  email_user?: any;
  password_user?: any;
};

export const action: ActionFunction = async ({ request }) => {
  try {
    const form = await request.formData();
    const email = form.get("email");
    const password = form.get("password");
    const phone = form.get("phone");
    const fullname = form.get("fullname");
    console.log(email, password, phone, fullname);
    const {
      email_user,
      password_user,
      phone_user,
      fullname_user,
      errContent,
      err,
    } = validation({
      email,
      password,
      phone,
      fullname,
    }) as Reg_Result;
    const data={
      email,
      password,
      phone,
      fullname
    }
    let error: { err: any; code: any; message: any; path: any,data:any }[] = [];
    if (errContent && errContent?.issues.length !== 0) {
      errContent.issues.map((p: any) => {
        let code = p.code;
        let message = p.message;
        let path = p.path[0];
        error.push({
          err,
          code,
          message,
          path,
          data
        });
      });
      return json({
        error:error,
      });
    }
    if (error.length == 0) {
      const data = {
        email: email_user,
        password: password_user,
        phone: phone_user,
        fullname: fullname_user,
      };
      twilioCustomPhoneRegistration({ phone: data.phone });
      return json({
        data,
      });
    }
    return error;
    //Hold the user and go to the verification page
  } catch (error) {
    // const err = error as Error;
    // throw err;
    console.log(error);
  }
};
export const loader: LoaderFunction = async (request) => {
  return {};
};

function signup() {
  const data = useActionData();
  const [phone,setPhone]=useState('')
  const [email,setEmail]=useState('')
  const [fullname,setFullname]=useState('')

  let user = data?.data;
  let validated_inputs:any[]=[]
  console.log("ERROR",data?.error)

  if(data?.error?.data){
   data?.error?.data.map((da:any) => {
     if (da.phone != "") {
       setPhone(da.phone);
     }
     if (da.email !== "") {
       setEmail(da.email);
     }
     if (da.fullname !== "") {
       setFullname(da.fullname);
     }
   });
  }
  return (
    <Box width={"100vw"} height={"100vh"} p={0} overflow="hidden">
      <Box
        display={"flex"}
        alignItems="center"
        justifyContent={"space-between"}
        width="100%"
        height="100%"
        position={"relative"}
      >
        {/* left side */}
        <Box
          flex={1}
          width={"100%"}
          height={"100%"}
          bgcolor="white"
          display="flex"
          flexDirection={"column"}
          justifyContent="center"
          overflow={"hidden"}
        >
          <Typography
            variant="h2"
            marginLeft="150px"
            color="darkblue"
            fontWeight={"bold"}
          >
            Blog Me
          </Typography>
          <Box
            width={"100%"}
            height="50%"
            display={"flex"}
            alignItems="center"
            marginLeft="100px"
            mt={10}
          >
            <img
              src="https://img.freepik.com/free-vector/access-control-system-abstract-concept_335657-3180.jpg?w=740&t=st=1665256374~exp=1665256974~hmac=751ff9fcd7534f59950c2d9a8d70502b6c7dbe4e636b6d8e1debe4a058faef30"
              alt=""
              style={{
                width: "400px",
                height: "400px",
              }}
            />
          </Box>
        </Box>
        {/* end left side  */}

        {/* Start Right side */}
        <Box
          flex={1}
          width={"50%"}
          height="100%"
          right="0px"
          position={"absolute"}
          display="flex"
          justifyContent={"center"}
          alignItems="center"
          boxShadow={"20px"}
          p={2}
          sx={{
            borderTopLeftRadius: "40px",
            borderBottomLeftRadius: "40px",
            backgroundImage: `url('https://images.unsplash.com/photo-1598027097863-319c291951a5?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=872&q=80')`,
          }}
        >
          <Box
            bgcolor={"inherit"}
            width="100%"
            height={"100%"}
            borderRadius="10px"
            padding="30px"
            display={"flex"}
            alignItems="center"
          >
            <Box
              width="100%"
              // height="400px"
              bgcolor="whitesmoke"
              p={10}
              // mt={10}
              display="flex"
              flexDirection={"column"}
              justifyContent="center"
              borderRadius="20px"
              alignItems={"center"}
            >
              <Box>
                <Typography
                  variant="h5"
                  sx={{
                    marginBottom: "20px",
                  }}
                >
                  Create Account
                </Typography>
              </Box>
              <form
                method="post"
                style={{
                  display: "flex",
                  flexDirection: "column",
                  width: "100%",
                }}
              >
                <Box display="flex" flexDirection={"column"} width="90%">
                  <TextField
                    // id="filled-basfsdic"
                    label="Email"
                    name="email"
                    variant="filled"
                    color="primary"
                    sx={{
                      marginBottom: "20px",
                      color: "white",
                    }}
                  />
                  {/* show vaidation Error  */}
                  {
                    data?.error?.map((p: any) => {
                      if (p.path == "email") {
                        return (
                          <Typography color="error">{`${p.path}: ${p.message}`}</Typography>
                        );
                      }
                    })}
                  <TextField
                    id="filled-basic1"
                    label="Phone Number"
                    name="phone"
                    variant="filled"
                    color="primary"
                    sx={{
                      marginBottom: "20px",
                      color: "white",
                    }}
                  />

                  {/* show vaidation Error  */}
                  { data?.error?.map((p: any) => {
                      if (p.path == "phone") {
                        return (
                          <Typography color="error">{`${p.path}: ${p.message}`}</Typography>
                        );
                      }
                    })}
                  <TextField
                    id="filled-basic"
                    label="Full Name"
                    name="fullname"
                    variant="filled"
                    color="primary"
                    sx={{
                      marginBottom: "20px",
                      color: "white",
                    }}
                  />

                  {/* show vaidation Error  */}
                  {
                    data?.error?.map((p: any) => {
                      if (p.path == "fullname") {
                        return (
                          <Typography color="error">{`${p.path}: ${p.message}`}</Typography>
                        );
                      }
                    })}
                  <TextField
                    id="filled-basic"
                    label="Password"
                    name="password"
                    variant="filled"
                    type={"password"}
                  />
                  {/* show vaidation Error  */}
                  {
                    data?.error?.map((p: any) => {
                      if (p.path == "password") {
                        return (
                          <Typography color="error">{`${p.path}: ${p.message}`}</Typography>
                        );
                      }
                    })}
                  {user == undefined && (
                    <Button
                      variant="contained"
                      type="submit"
                      sx={{
                        marginTop: "20px",
                      }}
                    >
                      Register
                    </Button>
                  )}

                  {user !== "" && user != undefined && (
                    <form
                      method="post"
                      action="/auth/phone_auth/verification"
                      style={{
                        display: "flex",
                        flexDirection: "column",
                        width: "100%",
                      }}
                    >
                      <input
                        type="hidden"
                        value={user?.password}
                        name="password"
                      />
                      <input type="hidden" value={user?.email} name="email" />
                      <input type="hidden" value={user?.phone} name="phone" />
                      <input type="hidden" value={user?.fullname} name="name" />
                      <Button
                        variant="contained"
                        type="submit"
                        sx={{
                          width: "100%",
                          marginTop: "20px",
                        }}
                      >
                        Give me verification Page
                      </Button>
                    </form>
                  )}
                  <Box display={"flex"} alignItems="center" mt={5}>
                    <Typography>Already Have an Account?</Typography>
                    <Typography
                      sx={{
                        marginLeft: "10px",
                      }}
                    >
                      <a href="/auth/login">Login</a>
                    </Typography>
                  </Box>
                </Box>
              </form>
            </Box>
          </Box>
        </Box>
      </Box>
      {/* End Right side */}
    </Box>
  );
}

export default signup;
