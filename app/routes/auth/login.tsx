import { Box, Button, InputBase, TextField, Typography } from "@mui/material";
import React from "react";
import { Google, LocalPhone } from "@mui/icons-material";
import type { ActionFunction, LoaderFunction } from "@remix-run/node";
import { json, redirect } from "@remix-run/node";
import { useActionData, useLoaderData } from "@remix-run/react";
import authenticator, { loggedUser, Result, user } from "~/server/auth.server";

export const action: ActionFunction = async ({ request, context }) => {
  return await authenticator.authenticate("form", request, {
    successRedirect: "/",
    failureRedirect: "/auth/login",
    throwOnError: true,
  });
};

export const loader: LoaderFunction = async ({ request }) => {
  let result = (await authenticator.isAuthenticated(request)) as loggedUser;
  console.log(result);
  if (result?.user && result !== null) {
    return redirect("/");
  }
  return json(result);
};

export default function Login() {
  const loader_data = useLoaderData();
  const action_data=useActionData()
  console.log("Action data",action_data)
  console.log("loader", loader_data);
  return (
    <Box width={"100vw"} height={"100vh"} p={0} overflow="hidden">
      <Box
        display={"flex"}
        alignItems="center"
        justifyContent={"space-between"}
        width="100%"
        height="100%"
        position={"relative"}
      >
        {/* left side */}
        <Box
          flex={1}
          width={"100%"}
          height={"100%"}
          bgcolor="white"
          display="flex"
          flexDirection={"column"}
          justifyContent="center"
          overflow={"hidden"}
        >
          <Typography
            variant="h2"
            marginLeft="150px"
            color="darkblue"
            fontWeight={"bold"}
          >
            Blog Me
          </Typography>
          <Box
            width={"100%"}
            height="50%"
            display={"flex"}
            alignItems="center"
            marginLeft="100px"
            mt={10}
          >
            <img
              src="https://img.freepik.com/free-vector/access-control-system-abstract-concept_335657-3180.jpg?w=740&t=st=1665256374~exp=1665256974~hmac=751ff9fcd7534f59950c2d9a8d70502b6c7dbe4e636b6d8e1debe4a058faef30"
              alt=""
              style={{
                width: "400px",
                height: "400px",
              }}
            />
          </Box>
        </Box>
        {/* end left side  */}

        {/* Start Right side */}
        <Box
          flex={1}
          width={"50%"}
          height="100%"
          right="0px"
          position={"absolute"}
          display="flex"
          justifyContent={"center"}
          alignItems="center"
          p={2}
          sx={{
            borderTopLeftRadius: "40px",
            borderBottomLeftRadius: "40px",
            backgroundImage: `url('https://images.unsplash.com/photo-1598027097863-319c291951a5?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=872&q=80')`,
          }}
        >
          <Box
            bgcolor={"inherit"}
            width="100%"
            height={"100%"}
            borderRadius="10px"
            padding="30px"
          >
            <Box marginBottom="50px"></Box>
            <Box
              display={"flex"}
              alignItems="center"
              width="100%"
              justifyContent={"space-evenly"}
            >
              {/* Buttons */}
              <Button
                variant="contained"
                sx={{
                  color: "white",
                  padding: "10px",
                  fontWeight: "bold",
                  width: "270px",
                }}
                disabled
                startIcon={
                  <Google
                    sx={{
                      color: "white",
                    }}
                  />
                }
              >
                <a
                  href="/auth/login"
                  style={{
                    textDecoration: "none",
                    color: "white",
                  }}
                >
                  SignIN With EMail
                </a>
              </Button>

              <Button
                variant="contained"
                startIcon={<LocalPhone />}
                sx={{
                  color: "white",
                  padding: "10px",
                  fontWeight: "bold",
                }}
              >
                <a
                  href="/auth/phone_auth"
                  style={{
                    textDecoration: "none",
                    color: "white",
                  }}
                >
                  SIGNIN WITH PHONE NUMBER
                </a>
              </Button>
            </Box>
            <Box
              width="100%"
              height="50px"
              mt={5}
              display="flex"
              justifyContent="center"
              alignItems={"center"}
            >
              <Typography textAlign={"center"}> - OR - </Typography>
            </Box>
            <Box
              width="100%"
              height="400px"
              boxShadow={"initial"}
              sx={{
                background: "whitesmoke",
              }}
              // mt={10}
              display="flex"
              justifyContent="center"
              flexDirection={"column"}
              borderRadius="20px"
              alignItems={"center"}
            >
              <Typography
                variant="h5"
                sx={{
                  marginBottom: "20px",
                }}
              >
                Sign In
              </Typography>

              <Box display="flex" flexDirection={"column"} width="70%">
                <form
                  method="post"
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    width: "100%",
                  }}
                >
                  <TextField
                    id="filled-basic"
                    label="Email"
                    variant="filled"
                    color="primary"
                    name="email"
                    sx={{
                      marginBottom: "20px",
                      color: "white",
                    }}
                  />
                  {loader_data?.error?.map((p: any, idx: any) => {
                    if (p.path == "email_user") {
                      return (
                        <Typography color={"error"} key={idx}>
                          {p.message}
                        </Typography>
                      );
                    }
                  })}
                  <TextField
                    id="filled-basiec"
                    label="Password"
                    variant="filled"
                    name="password"
                    type="password"
                  />
                  {loader_data?.error?.map((p: any, idx: any) => {
                    if (p.path == "password_user") {
                      return (
                        <Typography color={"error"} key={idx}>
                          {p.message}
                        </Typography>
                      );
                    }
                  })}
                  <Button
                    variant="contained"
                    type="submit"
                    sx={{
                      marginTop: "20px",
                    }}
                  >
                    Sign In
                  </Button>
                  {loader_data != null &&
                    loader_data?.message == "Invalid Credientials" && (
                      <Typography color={"error"}>
                        Invalid Email and Password
                      </Typography>
                    )}
                </form>
                <Box display={"flex"} alignItems="center" mt={5}>
                  <Typography>Don't have account yet?</Typography>
                  <Typography
                    sx={{
                      marginLeft: "10px",
                    }}
                  >
                    <a href="/auth/signup">Register</a>
                  </Typography>
                </Box>
                <Box width="100%">
                  {/* {data && data.error && (
                    <Typography>
                      You have error Form data {data?.error?.message}
                    </Typography>
                  )} */}
                </Box>
              </Box>

              {/*  */}
            </Box>
          </Box>
        </Box>
      </Box>
      {/* End Right side */}
    </Box>
  );
}

export function ErrorBoundary({ error }: { error: Error }) {
  console.log("Here is the errors", error);
  return (
    <Box
      width="200px"
      height="200px"
      display="flex"
      padding="10px"
      justifyContent="center"
      bgcolor="cyan"
      alignItems="center"
    >
      <Typography>We have got some problems in your app</Typography>
    </Box>
  );
}
