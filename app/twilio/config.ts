export const accountSid = process.env.TWILIO_ACCOUNT_SID;
export const authToken = process.env.TWILIO_AUTH_TOKEN;
export const verifySid = process.env.verifySid as string;
import { json } from "remix";
import twilio from "twilio";
const clientTwilio = twilio(accountSid, authToken);
