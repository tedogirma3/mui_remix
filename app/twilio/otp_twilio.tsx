import { json } from "remix";
import twilio from "twilio";
import { db } from "~/server/shared/db.server";
import { accountSid, authToken, verifySid } from "./config";
const clientTwilio = twilio(accountSid, authToken);
import bcrypt from "bcrypt";

export const twilioCustomPhoneRegistration = (data: object | any) => {
  const { phone } = data;
  console.log("Phone", phone);
  try {
    clientTwilio.verify.v2
      .services(verifySid)
      .verifications.create({
        to: `+251${Number(phone)}`,
        channel: "sms",
      })
      .then()
      .catch((e) => {
        const err = e as Error;
        throw err;
      });
    return true;
  } catch (error) {
    const e = error as Error;
    throw e;
  }
};

export const twilioCustomPhoneVerification = (data: object | string | any) => {
  try {
    const { phone, otp, name, password, email } = data;
    console.log(phone, otp, name, password, email);
    let user;
    clientTwilio.verify.v2
      .services(verifySid)
      .verificationChecks.create({ to: `+251${Number(phone)}`, code: otp })
      .then(async (verification_check) => {
        console.log("Verification check", verification_check);
        if (verification_check.status === "approved") {
          //find the number with her/his information
          user = db.user
            .create({
              data: {
                email: email,
                password: bcrypt.hashSync(password, 10),
                firstName: name,
                lastName: name,
                phoneNumber: phone,
                status: "ACTIVE",
              },
            })
            .then((e) => {
              return {
                email: e.email,
                Name: e.firstName,
                phone: e.phoneNumber,
                createdAt: e.createdAt,
              };
            });
        }
      });
    console.log("User", user);
    return user;
  } catch (error) {
    const err = error as Error;
    throw err;
  }
};
